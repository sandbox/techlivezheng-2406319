<?php
class WebformZipExporterArchiverZip extends ArchiverZip {
  /**
   * Implement the add function with the ability to not use the full filepath
   *
   * @param string $file_path
   * @param string|boolean $basename
   * @return \WebformZipExporterArchiverZip
   */
  public function add($file_path, $basename = FALSE) {
    if ($basename) {
      if (! is_string($basename)) {
        $basename = basename($file_path);
      }
      $this->zip->addFile($file_path, $basename);
    }
    else {
      $this->zip->addFile($file_path);
    }
    return $this;
  }
}
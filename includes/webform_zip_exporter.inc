<?php

/**
 * Base class defining the common methods available to exporters.
 */
class webform_zip_exporter extends webform_exporter_delimited {

  /**
   * Constructor for webform_exporter classes.
   *
   * @param $options
   *   The array of export options as provided by the user-interface.
   */
  function __construct($options) {
    $options['delimiter'] = ',';
    parent::__construct($options);
  }

  /**
   * Add a single row to the export file.
   *
   * @param $file_handle
   *   A PHP file handle to the export file.
   * @param array $data
   *   An array of formatted data for this row. One cell per item.
   * @param int $row_count
   *   The current number of rows in the export file.
   */
  function add_row(&$file_handle, $data, $row_count) {
    module_load_include('inc', 'webform_zip_exporter', 'includes/webform_zip_exporter.archiver');

    $zip_file = $_SESSION['webform_zip_exporter_zip_file'];

    $zip_object = new WebformZipExporterArchiverZip(drupal_realpath($zip_file));

    foreach ($data as $k => $v) {
      if (preg_match('/^(private:\/\/)|(public:\/\/)/', $v)) {
        if (file_exists($v = drupal_realpath($v))) {
          $f = 'assets/' . basename($v);

          $data[$k] = $f;

          $zip_object->add($v, $f);
        }
      }
    }

    $zip_object->getArchive()->close();

    parent::add_row($file_handle, $data, $row_count);
  }

  /**
   * Provide headers to the page when an export file is being downloaded.
   *
   * @param $filename
   *   The name of the file being downloaded. e.g. export.xls.
   */
  function set_headers($filename) {
    webform_exporter::set_headers($filename);
    drupal_add_http_header('Content-Type', 'application/x-zip-compressed');
    drupal_add_http_header('Content-Disposition', "attachment; filename=" . $filename . '.zip');
  }

  /**
   * Write the start of the export file.
   *
   * @param $file_handle
   *   A PHP file handle to the export file.
   */
  function bof(&$file_handle) {
    parent::bof($file_handle);

    $_SESSION['webform_zip_exporter_zip_file'] = _webform_export_tempname();
  }

  /**
   * Write the end of the export file.
   *
   * @param $file_handle
   *   A PHP file handle to the export file.
   */
  function eof(&$file_handle, $row_count, $col_count) {
    parent::eof($file_handle, $row_count, $col_count);
  }

  /**
   * Allow final processing of the results.
   *
   * @param $results
   *   An array of result data, including:
   *   - node: The node whose results are being downloaded.
   *   - file_name: The full file path of the generated file.
   *   - row_count: The final number of rows in the generated file.
   */
  function post_process(&$results) {
    parent::post_process($results);

    module_load_include('inc', 'webform_zip_exporter', 'includes/webform_zip_exporter.archiver');

    $zip_file = $_SESSION['webform_zip_exporter_zip_file'];

    $zip_object = new WebformZipExporterArchiverZip(drupal_realpath($zip_file));

    $zip_object->add(drupal_realpath($results['file_name']), $results['node']->title . '.csv');

    $zip_object->getArchive()->close();

    @unlink($results['file_name']);  // Clean up, the @ makes it silent.

    unset($_SESSION['webform_zip_exporter_zip_file']);

    $results['file_name'] = $zip_file;
  }
}
